/*

	Problem:
	If we list all the natural numbers below 10 that are multiples of 3 or 5, we
   	get 3, 5, 6 and 9. The sum of these multiples is 23.

	Find the sum of all the multiples of 3 or 5 below 1000.

*/

var n = parseInt(process.argv[2], 10);

var allNaturalNumbers = [];

for(var i=1; i < n; i++) {
	allNaturalNumbers.push(i);
}

var multiples = allNaturalNumbers.filter(function(x) {return (x % 5 == 0) || (x % 3 == 0);});

var result = multiples.reduce(function (a, b) {return a + b;});

console.log("Result: " + result.toString());
