﻿(*
    Problem:

    By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

    What is the 10 001st prime number?

*)

module Problem7

let solveFor n =
    let isFactor n d =
        n % d = 0

    let isPrime n =

        let squareRoot = (int <| sqrt (float n) ) + 1

        let divisors =
            Seq.init squareRoot (fun x -> squareRoot - x ) |>
            Seq.filter (fun x -> x <> 1)

        let factors =
            divisors |>
            Seq.filter (isFactor n)

        not (factors |> Seq.exists (fun _ -> true))

    let primes =
        Seq.unfold (fun lastPrime ->
            let integerSequence = Seq.initInfinite (fun x -> x + lastPrime + 1)
            let nextPrime =
                integerSequence |>
                Seq.find (fun x -> isPrime x)
            Some(nextPrime, nextPrime)
        ) 1

    primes |> Seq.skip (n - 2) |> Seq.head
