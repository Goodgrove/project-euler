﻿namespace ProjectEuler.Tests.Problem8

open NUnit.Framework

[<TestFixture>]
[<Category("Specification")>]
type SpecificationTests() = class
    [<Test>]
    member self.Problem8() =
        Assert.AreEqual(5832, (Problem8.solveFor 4))
end

[<TestFixture>]
[<Category("Answer")>]
type AnswerTests() = class
    [<Test>]
    member self.Problem8() =
        Assert.AreEqual(23514624000L, (Problem8.solveFor 13))
end
