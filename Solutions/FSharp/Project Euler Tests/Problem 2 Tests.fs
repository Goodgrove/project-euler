﻿namespace ProjectEuler.Tests.Problem2

open NUnit.Framework

[<TestFixture>]
[<Category("Specification")>]
type SpecificationTests() = class
    [<Test>]
    member self.SpecificationTest() =
        let result = Problem2.solveFor 90
        Assert.AreEqual (44, result)
end

[<TestFixture>]
[<Category("Answer")>]
type AnswersTests() = class

    [<Test>]
    member self.AnswerTest() =
        let result = Problem2.solveFor 4000000
        Assert.AreEqual (4613732, result)
end


