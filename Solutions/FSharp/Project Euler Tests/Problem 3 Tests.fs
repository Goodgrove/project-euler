﻿namespace ProjectEuler.Tests.Problem3

open NUnit.Framework

[<TestFixture>]
[<Category("Unit")>]
type UnitTests() = class
    [<TestCase(2)>]
    [<TestCase(28)>]
    [<TestCase(33)>]
    [<TestCase(50)>]
    member self.Problem3PrimesReturnFalse(n) =
        Assert.IsFalse (Problem3.isPrime n)
end

[<TestFixture>]
[<Category("Specification")>]
type SpecificationTests() = class
    [<TestCase(5)>]
    [<TestCase(7)>]
    [<TestCase(13)>]
    [<TestCase(29)>]
    member self.Problem3PrimesReturnTrue(n) =
        Assert.IsTrue (Problem3.isPrime n)
end

