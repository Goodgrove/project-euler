﻿namespace ProjectEuler.Tests.Problem5

open NUnit.Framework

[<TestFixture>]
[<Category("Specification")>]
type SpecificationTests() = class
    [<Test>]
    member self.Problem5() =
        Assert.AreEqual(2520, (Problem5.solveFor 1 10))
end

[<TestFixture>]
[<Category("Answer")>]
[<Category("LongRunning")>]
type AnswerTests() = class
    [<Test>]
    member self.Problem5() =
        Assert.AreEqual(232792560, (Problem5.solveFor 1 20))
end
