﻿namespace ProjectEuler.Tests.Problem1

open NUnit.Framework

[<TestFixture>]
[<Category("Specification")>]
type SpecificationTests() = class

    [<Test>]
    member self.SpecificationTest() =
        let result = Problem1.solveFor 10
        Assert.AreEqual (23, result)

end

[<TestFixture>]
[<Category("Answer")>]
type AnswersTests() = class

    [<Test>]
    member self.Problem1() =
        let result = Problem1.solveFor 1000
        Assert.AreEqual (233168, result)
end



