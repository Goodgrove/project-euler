﻿namespace ProjectEuler.Tests.Problem7

open NUnit.Framework

[<TestFixture>]
[<Category("Specification")>]
type SpecificationTests() = class
    [<Test>]
    member self.Problem7() =
        Assert.AreEqual(13, (Problem7.solveFor 6))
end

[<TestFixture>]
[<Category("Answer")>]
type AnswerTests() = class
    [<Test>]
    member self.Problem7() =
        Assert.AreEqual(104743, (Problem7.solveFor 10001))
end
