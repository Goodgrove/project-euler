﻿namespace ProjectEuler.Tests.Problem6

open NUnit.Framework

[<TestFixture>]
[<Category("Specification")>]
type SpecificationTests() = class
    [<Test>]
    member self.Problem6() =
        Assert.AreEqual(2640, (Problem6.solveFor 10))
end

[<TestFixture>]
[<Category("Answer")>]
type AnswerTests() = class
    [<Test>]
    member self.Problem6() =
        Assert.AreEqual(25164150, (Problem6.solveFor 100))
end
