﻿namespace ProjectEuler.Tests.Problem4

open NUnit.Framework

[<TestFixture>]
[<Category("Specification")>]
type SpecificationTests() = class
    [<Test>]
    member self.Problem4() =
        Assert.AreEqual(9009, (Problem4.solveFor 2))
end

[<TestFixture>]
[<Category("Answer")>]
type AnswersTests() = class
    [<Test>]
    member self.Problem4() =
        Assert.AreEqual(906609, (Problem4.solveFor 3))
end
