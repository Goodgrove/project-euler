﻿(*
    Problem:



    The sum of the squares of the first ten natural numbers is,
    1^2 + 2^2 + ... + 10^2 = 385

    The square of the sum of the first ten natural numbers is,
    (1 + 2 + ... + 10)^2 = 55^2 = 3025

    Hence the difference between the sum of the squares of the first ten natural
    numbers and the square of the sum is 3025 − 385 = 2640.

    Find the difference between the sum of the squares of the first one hundred
    natural numbers and the square of the sum.
*)

module Problem6

let solveFor n =

    let naturalNumbers = List.init n (fun x -> x + 1)

    let sumOfSquares =
        naturalNumbers |>
        List.map (fun x -> x * x) |>
        List.sum

    let squareOfSum =
        int ((float (List.sum naturalNumbers)) ** 2.0)

    squareOfSum - sumOfSquares