﻿(*
    Problem:

    2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

    What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*)

module Problem5

let largestProduct digits =
    digits * digits

let isFactor n d =
    n % d = 0

let solveFor startNo endNo =
    let intList = List.rev <| List.init (endNo - startNo) (fun x -> x + startNo)
    Seq.initInfinite (fun i -> i + 1) |>
    Seq.skipWhile (fun x ->
        not (Seq.forall (fun xx -> isFactor x xx) intList)
    ) |>
    Seq.head