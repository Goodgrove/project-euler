﻿(*
    Problem:

    A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

    Find the largest palindrome made from the product of two 3-digit numbers.
*)

module Problem4

let largestProduct digits =
    digits * digits

let palindromicNumbers largestNumber =
    Seq.unfold (fun a ->
        if (a > 0) then
            Some (a-1, a-1)
        else
            None) largestNumber |>
    Seq.filter (fun a ->
        let astr = a.ToString()
        let astrRev = new string(Array.rev <| astr.ToCharArray())
        astr = astrRev
        )

let isFactor n d =
    n % d = 0

let intSqrt =
    (+) 1 << int << sqrt << float

let factorPairs n =
    let sqrtOfN = intSqrt n
    { sqrtOfN .. -1 .. 1 } |>
    Seq.filter (isFactor n) |>
    Seq.map (fun x -> (x, n / x))

let solveFor n =

    let largestNDigitNumber =
        (int (10.0 ** (float n))) - 1

    let isNDigits x =
        x >= (int (10.0 ** (float (n - 1)))) &&
        x < (int (10.0 ** (float n)))

    palindromicNumbers (largestProduct largestNDigitNumber) |>
    Seq.filter (fun x ->
        factorPairs x |>
        //Seq.takeWhile (fun (a, b) -> isNDigits a) |>
        Seq.filter (fun (a, b) -> (isNDigits a) && (isNDigits b)) |>
        Seq.exists (fun x -> true)
    ) |>
    Seq.take 1 |>
    Seq.exactlyOne
    