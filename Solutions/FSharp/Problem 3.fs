(*
    Problem:

    The prime factors of 13195 are 5, 7, 13 and 29.

    What is the largest prime factor of the number 600851475143 ?
*)

module Problem3

let isFactor n d =
    n % d = 0

let isPrime n =

    let squareRoot = (int <| sqrt (float n) ) + 1

    let divisors =
        Seq.init squareRoot (fun x -> squareRoot - x ) |>
        Seq.filter (fun x -> x <> 1)

    let factors =
        divisors |>
        Seq.filter (isFactor n)

    not (factors |> Seq.exists (fun _ -> true))
