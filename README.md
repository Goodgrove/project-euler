# Project-Euler

These are my solutions to the https://projecteuler.net/ problems.

My original intention for this repository is to use Project Euler
as a learning experience for lanaguage that I may not be 100% comfortable with.

I have started this repository with solutions for JavaScript and F#.
